/**
 * File:   ErrorStack.h
 * @author Oliver Paraskos 13074673
 *
 * Handler class for error messages, 
 * extensible to log to file/stderr, or do anything with the error messages.
 * 
 * Created on 06 November 2013, 11:06
 */

#ifndef ERRORSTACK_H
#define	ERRORSTACK_H

#include "Singleton.hpp"
#include <stack>
#include <string>

/** Enumerable Error Codes */
enum error_code
{
    ERR_NO_SUCH_CITY,
    ERR_INVALID_OPTION,
    ERR_SYNTAX_ERR,
    ERR_COULD_NOT_RENAME,
    ERR_ALREADY_EXISTS,
    ERR_FILE_ERROR,
    ERR_UNDEFINED
};

/**
 * get a string representation of the error 
 *
 * @param e what kind of error is this?
 * @return string that can be outputted to an error log.
 */
std::string toString(error_code e);

/**
 * get a string representation of the error 
 *
 * @param e what kind of error is this?
 * @param suffix what to put in after of error message
 * @return string that can be outputted to an error log.
 */
std::string toString(error_code e, std::string sufix);

/**
 * get a string representation of the error 
 *
 * @param e what kind of error is this?
 * @param prefix what to put in before of error message
 * @param suffix what to put in after of error message
 * @return string that can be outputted to an error log.
 */
std::string toString(error_code e, std::string prefix, std::string suffix);

  
class ErrorStack : public Singleton< ErrorStack >{
public:
    class error_t{        
    public:
        error_t()
            :error(ERR_UNDEFINED),
            message(""){}
            
        error_t(error_code e, std::string & s)
            : error(e),
            message(s) {}
            
        error_t(error_code e) 
            :error(e){
            message= ::toString(error);
        }
        
        std::string getMessage(); /** Get the error message **/
        void setMessage(std::string);/** Set the error message **/
        error_code getError();/** Get the error code **/
        void setError(error_code e);/** Set the error code **/
                
    public:
        enum error_code error;
    private:
        std::string message;
    };
    
    /**
     * Push an error to the top of the stack
     * @param e error to push on to the stack
     */
    void push(error_t e);
    
    /**
     * Push an error to the top of the stack
     * @param e error to push on to the stack
     */
    void push(error_code e);
    
    /**
     * Push an error to the top of the stack
     * @param e error to push on to the stack
     * @param message override the string relating to the error.
     */
    void push(error_code e, std::string message);
    
    /**
     * take 1 off the top of the stack.
     * @return the last element (just removed)
     */
    error_t pop();
    
    /**
     * read 1 off the top of the stack. without removing it
     * @return the last element on the stack.
     */
    error_t readLast();
    
    /**
     * @return the number of elements
     */
    size_t size();
    
    /**
     * take all the elements off the stack.
     */
    void clear();
    
private:
    /** the actual error ctack data structure as an STL stack */
    std::stack<error_t> errorStack;
};
/* conflicts with errno.h 
typedef ErrorStack::error_t error_t;
*/

#endif	/* ERRORSTACK_H */

