/* 
 * File:   CustomAssert.h
 * Author: Oliver Paraskos
 * 
 * Replacement Assert to give line and file names and numbers, functions etc
 * Allows you to disable assertions by removing -DDEBUG
 * and allows you to check for side affects with -DCHECK_SIDE_AFFECTS if code 
 * is no longer working with assertions off to see if its a side affect of
 * expressions with side affects.
 * 
 * Created on 16 October 2013, 13:26
 */

#ifndef CUSTOMASSERT_H
#define	CUSTOMASSERT_H

#ifdef NDEBUG
#undef DEBUG
#endif

#include <iostream>

#ifdef DEBUG
    /**! cassert
     * Custom assert function
     * Evaluates the expression logs an error and 'int 3's
     */
    inline void cassert( bool expr, const char* message, 
            const char* file = "", int line = 0, const char* function = "")
    {
            if( !expr ){
                //Print the problem
                std::cerr
                    << "Assertion Failed! : " << std::endl
                    << file << " @ line " << line << ": " << std::endl
                    << "in '" << function << "();'" << std::endl
                    << "\"" << message << "\"" << std::endl;

                // Trigger breakpoint:
                __asm__("int $3");
            }
    }

    /**! __cassert
     * this #define means you only have to type the expression into cassert
     */
    #define __cassert(expr) do{cassert(expr,#expr,__FILE__,__LINE__,__FUNCTION__);}while(0)

#else  /* RELEASE */
    #ifdef CHECK_SIDEAFFECTS
        inline void cassert( bool expr, const char* message, 
                const char* file = "", int line = 0, const char* function = "")
        {}

        #define __cassert(expr) do{(expr);} while(0)
    #else  /* DONT_CHECK_SIDEAFFECTS */
        inline void cassert( bool expr, const char* message, 
                const char* file = "", int line = 0, const char* function = "")
        {}
        
        #define __cassert(expr) do{__asm__("nop");} while(0)
    #endif  /* CHECK_SIDEAFFECTS / DONT_CHECK_SIDEAFFECTS  */

#endif  /* DEBUG / RELEASE*/
#endif	/* CUSTOMASSERT_H */

