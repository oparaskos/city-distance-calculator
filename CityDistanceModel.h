/* City Distance Model Class Header File
 * This header file contains the information of what will be in the city distance model class.
 * Author:
 * Emogene Atkinson
 * Katie Jarvis
 */
#ifndef CITYDISTANCEMODEL_H
#define CITYDISTANCEMODEL_H

#include "Model.h"
#include "CityLoader.h"
#include "City.h"
#include <string>
#include <iostream>
#include "CommandLineView.h"
 
class CityDistanceModel : public Model {
public:
        CityDistanceModel(): mCityMap()
        {
        }
        virtual ~CityDistanceModel()
        {
        }
        
        //void parseMessage(std::string s){}
        
        /**! addCity(City)
         * \brief Add a city to the database
         */
        bool addCity(City city);
        
        /**! removeCity(City)
         * \breif Remove City from the database
         */
        bool removeCity(City city);
        
        /**! modifyCity(String, City)
         * \breif Change details in the city, this is implemented as deleting and 
         * creating a new city with new details.
         * 0 or empty string is invalid values, these will be replaced with the 
         * values from the old city.
         */
        bool modifyCity(std::string, City);
        
        bool loadCities(std::string path);
        bool saveCities(std::string path);
        
        /**! findCity(String)
         * \brief find a city, return an iterator to the city.
         */
        CityLoader::citymap::iterator findCity(string cityName);
        
        /**! getDistance(String, String)
         * \breif Get the distance between 2 cities by name
         */
        double getDistance(string cityA, string cityB);
        
        /**! showCityInfo(String)
         * \brief print info about the city to the view (TODO:)
         */
        void showCityInfo(string cityName);
        
        /**! showAll()
         * \breif print city names and coordinates in a list
         */
        void showAll();
        
        /**! end() \breif get the end iterator of the city map*/
        CityLoader::citymap::iterator end();
        
        /**! begin() \breif get the begin iterator of the city map*/
        CityLoader::citymap::iterator begin();
        
        unsigned int size();

private:
        CityLoader::citymap mCityMap; //! the map containing all the cities
};

#endif