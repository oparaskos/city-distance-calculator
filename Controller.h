/* Controller Header File
 * Author:
 * Katie Jarvis
 */
#ifndef CONTROLLER_H
#define CONTROLLER_H

#include "Singleton.hpp"

class Controller : public Singleton<Controller>
{
public:
    Controller() : Singleton<Controller>() {}
    virtual ~Controller(){}
    
    virtual void control() = 0;
};

#endif