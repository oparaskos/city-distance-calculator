#
# Generated Makefile - do not edit!
#
# Edit the Makefile in the project folder instead (../Makefile). Each target
# has a -pre and a -post target defined where you can add customized code.
#
# This makefile implements configuration specific macros and targets.


# Environment
MKDIR=mkdir
CP=cp
GREP=grep
NM=nm
CCADMIN=CCadmin
RANLIB=ranlib
CC=gcc
CCC=g++
CXX=g++
FC=gfortran
AS=as

# Macros
CND_PLATFORM=Cygwin_4.x-Windows
CND_DLIB_EXT=dll
CND_CONF=Debug
CND_DISTDIR=dist
CND_BUILDDIR=build

# Include project Makefile
include Makefile

# Object Directory
OBJECTDIR=${CND_BUILDDIR}/${CND_CONF}/${CND_PLATFORM}

# Object Files
OBJECTFILES= \
	${OBJECTDIR}/City.o \
	${OBJECTDIR}/CityDistanceModel.o \
	${OBJECTDIR}/CityLoader.o \
	${OBJECTDIR}/CityMaths.o \
	${OBJECTDIR}/CommandLineController.o \
	${OBJECTDIR}/CommandLineView.o \
	${OBJECTDIR}/ErrorStack.o \
	${OBJECTDIR}/custom_hashtable.o \
	${OBJECTDIR}/main.o

# Test Directory
TESTDIR=${CND_BUILDDIR}/${CND_CONF}/${CND_PLATFORM}/tests

# Test Files
TESTFILES= \
	${TESTDIR}/TestFiles/f2

# C Compiler Flags
CFLAGS=

# CC Compiler Flags
CCFLAGS=-std=c++11
CXXFLAGS=-std=c++11

# Fortran Compiler Flags
FFLAGS=

# Assembler Flags
ASFLAGS=

# Link Libraries and Options
LDLIBSOPTIONS=

# Build Targets
.build-conf: ${BUILD_SUBPROJECTS}
	"${MAKE}"  -f nbproject/Makefile-${CND_CONF}.mk ${TESTDIR}/TestFiles/f1.exe

${TESTDIR}/TestFiles/f1.exe: ${OBJECTFILES}
	${MKDIR} -p ${TESTDIR}/TestFiles
	${LINK.cc} -o ${TESTDIR}/TestFiles/f1 ${OBJECTFILES} ${LDLIBSOPTIONS}

${OBJECTDIR}/City.o: City.cpp 
	${MKDIR} -p ${OBJECTDIR}
	${RM} $@.d
	$(COMPILE.cc) -g -Wall -DDEBUG -I. -std=c++11 -MMD -MP -MF $@.d -o ${OBJECTDIR}/City.o City.cpp

${OBJECTDIR}/CityDistanceModel.o: CityDistanceModel.cpp 
	${MKDIR} -p ${OBJECTDIR}
	${RM} $@.d
	$(COMPILE.cc) -g -Wall -DDEBUG -I. -std=c++11 -MMD -MP -MF $@.d -o ${OBJECTDIR}/CityDistanceModel.o CityDistanceModel.cpp

${OBJECTDIR}/CityLoader.o: CityLoader.cpp 
	${MKDIR} -p ${OBJECTDIR}
	${RM} $@.d
	$(COMPILE.cc) -g -Wall -DDEBUG -I. -std=c++11 -MMD -MP -MF $@.d -o ${OBJECTDIR}/CityLoader.o CityLoader.cpp

${OBJECTDIR}/CityMaths.o: CityMaths.cpp 
	${MKDIR} -p ${OBJECTDIR}
	${RM} $@.d
	$(COMPILE.cc) -g -Wall -DDEBUG -I. -std=c++11 -MMD -MP -MF $@.d -o ${OBJECTDIR}/CityMaths.o CityMaths.cpp

${OBJECTDIR}/CommandLineController.o: CommandLineController.cpp 
	${MKDIR} -p ${OBJECTDIR}
	${RM} $@.d
	$(COMPILE.cc) -g -Wall -DDEBUG -I. -std=c++11 -MMD -MP -MF $@.d -o ${OBJECTDIR}/CommandLineController.o CommandLineController.cpp

${OBJECTDIR}/CommandLineView.o: CommandLineView.cpp 
	${MKDIR} -p ${OBJECTDIR}
	${RM} $@.d
	$(COMPILE.cc) -g -Wall -DDEBUG -I. -std=c++11 -MMD -MP -MF $@.d -o ${OBJECTDIR}/CommandLineView.o CommandLineView.cpp

${OBJECTDIR}/ErrorStack.o: ErrorStack.cpp 
	${MKDIR} -p ${OBJECTDIR}
	${RM} $@.d
	$(COMPILE.cc) -g -Wall -DDEBUG -I. -std=c++11 -MMD -MP -MF $@.d -o ${OBJECTDIR}/ErrorStack.o ErrorStack.cpp

${OBJECTDIR}/custom_hashtable.o: custom_hashtable.cpp 
	${MKDIR} -p ${OBJECTDIR}
	${RM} $@.d
	$(COMPILE.cc) -g -Wall -DDEBUG -I. -std=c++11 -MMD -MP -MF $@.d -o ${OBJECTDIR}/custom_hashtable.o custom_hashtable.cpp

${OBJECTDIR}/main.o: main.cpp 
	${MKDIR} -p ${OBJECTDIR}
	${RM} $@.d
	$(COMPILE.cc) -g -Wall -DDEBUG -I. -std=c++11 -MMD -MP -MF $@.d -o ${OBJECTDIR}/main.o main.cpp

# Subprojects
.build-subprojects:

# Build Test Targets
.build-tests-conf: .build-conf ${TESTFILES}
${TESTDIR}/TestFiles/f2: ${TESTDIR}/tests/testsuite.o ${OBJECTFILES:%.o=%_nomain.o}
	${MKDIR} -p ${TESTDIR}/TestFiles
	${LINK.cc}   -o ${TESTDIR}/TestFiles/f2 $^ ${LDLIBSOPTIONS} 


${TESTDIR}/tests/testsuite.o: tests/testsuite.cpp 
	${MKDIR} -p ${TESTDIR}/tests
	${RM} $@.d
	$(COMPILE.cc) -g -Wall -DDEBUG -I. -I. -I. -I. -I. -I. -I. -I. -I. -I. -I. -I. -std=c++11 -MMD -MP -MF $@.d -o ${TESTDIR}/tests/testsuite.o tests/testsuite.cpp


${OBJECTDIR}/City_nomain.o: ${OBJECTDIR}/City.o City.cpp 
	${MKDIR} -p ${OBJECTDIR}
	@NMOUTPUT=`${NM} ${OBJECTDIR}/City.o`; \
	if (echo "$$NMOUTPUT" | ${GREP} '|main$$') || \
	   (echo "$$NMOUTPUT" | ${GREP} 'T main$$') || \
	   (echo "$$NMOUTPUT" | ${GREP} 'T _main$$'); \
	then  \
	    ${RM} $@.d;\
	    $(COMPILE.cc) -g -Wall -DDEBUG -I. -std=c++11 -Dmain=__nomain -MMD -MP -MF $@.d -o ${OBJECTDIR}/City_nomain.o City.cpp;\
	else  \
	    ${CP} ${OBJECTDIR}/City.o ${OBJECTDIR}/City_nomain.o;\
	fi

${OBJECTDIR}/CityDistanceModel_nomain.o: ${OBJECTDIR}/CityDistanceModel.o CityDistanceModel.cpp 
	${MKDIR} -p ${OBJECTDIR}
	@NMOUTPUT=`${NM} ${OBJECTDIR}/CityDistanceModel.o`; \
	if (echo "$$NMOUTPUT" | ${GREP} '|main$$') || \
	   (echo "$$NMOUTPUT" | ${GREP} 'T main$$') || \
	   (echo "$$NMOUTPUT" | ${GREP} 'T _main$$'); \
	then  \
	    ${RM} $@.d;\
	    $(COMPILE.cc) -g -Wall -DDEBUG -I. -std=c++11 -Dmain=__nomain -MMD -MP -MF $@.d -o ${OBJECTDIR}/CityDistanceModel_nomain.o CityDistanceModel.cpp;\
	else  \
	    ${CP} ${OBJECTDIR}/CityDistanceModel.o ${OBJECTDIR}/CityDistanceModel_nomain.o;\
	fi

${OBJECTDIR}/CityLoader_nomain.o: ${OBJECTDIR}/CityLoader.o CityLoader.cpp 
	${MKDIR} -p ${OBJECTDIR}
	@NMOUTPUT=`${NM} ${OBJECTDIR}/CityLoader.o`; \
	if (echo "$$NMOUTPUT" | ${GREP} '|main$$') || \
	   (echo "$$NMOUTPUT" | ${GREP} 'T main$$') || \
	   (echo "$$NMOUTPUT" | ${GREP} 'T _main$$'); \
	then  \
	    ${RM} $@.d;\
	    $(COMPILE.cc) -g -Wall -DDEBUG -I. -std=c++11 -Dmain=__nomain -MMD -MP -MF $@.d -o ${OBJECTDIR}/CityLoader_nomain.o CityLoader.cpp;\
	else  \
	    ${CP} ${OBJECTDIR}/CityLoader.o ${OBJECTDIR}/CityLoader_nomain.o;\
	fi

${OBJECTDIR}/CityMaths_nomain.o: ${OBJECTDIR}/CityMaths.o CityMaths.cpp 
	${MKDIR} -p ${OBJECTDIR}
	@NMOUTPUT=`${NM} ${OBJECTDIR}/CityMaths.o`; \
	if (echo "$$NMOUTPUT" | ${GREP} '|main$$') || \
	   (echo "$$NMOUTPUT" | ${GREP} 'T main$$') || \
	   (echo "$$NMOUTPUT" | ${GREP} 'T _main$$'); \
	then  \
	    ${RM} $@.d;\
	    $(COMPILE.cc) -g -Wall -DDEBUG -I. -std=c++11 -Dmain=__nomain -MMD -MP -MF $@.d -o ${OBJECTDIR}/CityMaths_nomain.o CityMaths.cpp;\
	else  \
	    ${CP} ${OBJECTDIR}/CityMaths.o ${OBJECTDIR}/CityMaths_nomain.o;\
	fi

${OBJECTDIR}/CommandLineController_nomain.o: ${OBJECTDIR}/CommandLineController.o CommandLineController.cpp 
	${MKDIR} -p ${OBJECTDIR}
	@NMOUTPUT=`${NM} ${OBJECTDIR}/CommandLineController.o`; \
	if (echo "$$NMOUTPUT" | ${GREP} '|main$$') || \
	   (echo "$$NMOUTPUT" | ${GREP} 'T main$$') || \
	   (echo "$$NMOUTPUT" | ${GREP} 'T _main$$'); \
	then  \
	    ${RM} $@.d;\
	    $(COMPILE.cc) -g -Wall -DDEBUG -I. -std=c++11 -Dmain=__nomain -MMD -MP -MF $@.d -o ${OBJECTDIR}/CommandLineController_nomain.o CommandLineController.cpp;\
	else  \
	    ${CP} ${OBJECTDIR}/CommandLineController.o ${OBJECTDIR}/CommandLineController_nomain.o;\
	fi

${OBJECTDIR}/CommandLineView_nomain.o: ${OBJECTDIR}/CommandLineView.o CommandLineView.cpp 
	${MKDIR} -p ${OBJECTDIR}
	@NMOUTPUT=`${NM} ${OBJECTDIR}/CommandLineView.o`; \
	if (echo "$$NMOUTPUT" | ${GREP} '|main$$') || \
	   (echo "$$NMOUTPUT" | ${GREP} 'T main$$') || \
	   (echo "$$NMOUTPUT" | ${GREP} 'T _main$$'); \
	then  \
	    ${RM} $@.d;\
	    $(COMPILE.cc) -g -Wall -DDEBUG -I. -std=c++11 -Dmain=__nomain -MMD -MP -MF $@.d -o ${OBJECTDIR}/CommandLineView_nomain.o CommandLineView.cpp;\
	else  \
	    ${CP} ${OBJECTDIR}/CommandLineView.o ${OBJECTDIR}/CommandLineView_nomain.o;\
	fi

${OBJECTDIR}/ErrorStack_nomain.o: ${OBJECTDIR}/ErrorStack.o ErrorStack.cpp 
	${MKDIR} -p ${OBJECTDIR}
	@NMOUTPUT=`${NM} ${OBJECTDIR}/ErrorStack.o`; \
	if (echo "$$NMOUTPUT" | ${GREP} '|main$$') || \
	   (echo "$$NMOUTPUT" | ${GREP} 'T main$$') || \
	   (echo "$$NMOUTPUT" | ${GREP} 'T _main$$'); \
	then  \
	    ${RM} $@.d;\
	    $(COMPILE.cc) -g -Wall -DDEBUG -I. -std=c++11 -Dmain=__nomain -MMD -MP -MF $@.d -o ${OBJECTDIR}/ErrorStack_nomain.o ErrorStack.cpp;\
	else  \
	    ${CP} ${OBJECTDIR}/ErrorStack.o ${OBJECTDIR}/ErrorStack_nomain.o;\
	fi

${OBJECTDIR}/custom_hashtable_nomain.o: ${OBJECTDIR}/custom_hashtable.o custom_hashtable.cpp 
	${MKDIR} -p ${OBJECTDIR}
	@NMOUTPUT=`${NM} ${OBJECTDIR}/custom_hashtable.o`; \
	if (echo "$$NMOUTPUT" | ${GREP} '|main$$') || \
	   (echo "$$NMOUTPUT" | ${GREP} 'T main$$') || \
	   (echo "$$NMOUTPUT" | ${GREP} 'T _main$$'); \
	then  \
	    ${RM} $@.d;\
	    $(COMPILE.cc) -g -Wall -DDEBUG -I. -std=c++11 -Dmain=__nomain -MMD -MP -MF $@.d -o ${OBJECTDIR}/custom_hashtable_nomain.o custom_hashtable.cpp;\
	else  \
	    ${CP} ${OBJECTDIR}/custom_hashtable.o ${OBJECTDIR}/custom_hashtable_nomain.o;\
	fi

${OBJECTDIR}/main_nomain.o: ${OBJECTDIR}/main.o main.cpp 
	${MKDIR} -p ${OBJECTDIR}
	@NMOUTPUT=`${NM} ${OBJECTDIR}/main.o`; \
	if (echo "$$NMOUTPUT" | ${GREP} '|main$$') || \
	   (echo "$$NMOUTPUT" | ${GREP} 'T main$$') || \
	   (echo "$$NMOUTPUT" | ${GREP} 'T _main$$'); \
	then  \
	    ${RM} $@.d;\
	    $(COMPILE.cc) -g -Wall -DDEBUG -I. -std=c++11 -Dmain=__nomain -MMD -MP -MF $@.d -o ${OBJECTDIR}/main_nomain.o main.cpp;\
	else  \
	    ${CP} ${OBJECTDIR}/main.o ${OBJECTDIR}/main_nomain.o;\
	fi

# Run Test Targets
.test-conf:
	@if [ "${TEST}" = "" ]; \
	then  \
	    ${TESTDIR}/TestFiles/f2 || true; \
	else  \
	    ./${TEST} || true; \
	fi

# Clean Targets
.clean-conf: ${CLEAN_SUBPROJECTS}
	${RM} -r ${CND_BUILDDIR}/${CND_CONF}
	${RM} ${TESTDIR}/TestFiles/f1.exe

# Subprojects
.clean-subprojects:

# Enable dependency checking
.dep.inc: .depcheck-impl

include .dep.inc
