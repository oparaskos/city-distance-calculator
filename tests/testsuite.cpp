/* 
 * File:   newsimpletest1.cpp
 * Author: Paraskos
 *
 * Created on 16-Oct-2013, 13:43:22
 */

#include <stdlib.h>
#include <iostream>
#include "City.h"
#include "CityMaths.h"
#include "CityLoader.h"
#include "CityDistanceModel.h"

/*
 * Asserion checking:
 * disabled because of 'int 3'
 *
 void assertionCheck(){
    //__cassert("This should not be an error");
    //__cassert(!"This should be an error");
    //__cassert(!false && "This should not be an error");
    //__cassert(!true && "This should be an error");
    
    int * pMyInt = NULL;
    
    //this should not run if asserts are disabled its bad code, but if someone 
    //    makes a mistake they should be able to enable 
    //    "CHECK_SIDEAFFECTS" to find if the bug is one of these
    
    __cassert( (pMyInt = new int(10)) && pMyInt != NULL );
    
    if(!pMyInt){
        std::cout << "pMyInt was NULL" <<std::endl;
    }else {
        std::cout << "pMyInt was Not NULL" << std::endl;  
    }
    delete pMyInt;
 }
 */


void createCityTest()
{
    std::cout << "CITY CREATION TEST" << std::endl;
    const double lon = 51.507222;
    const double lat = -0.1275;
    const std::string name("London");
    
    City a;
    a.setName(name);
    a.setLatitude(lat);
    a.setLongitude(lon);
    
    if(a.getName().compare(name))
    {
        std::cout << "a.getName() = '" << a.getName() << "'" << std::endl
                  << "input was '" << name << "'" << std::endl;
        std::cout << "%TEST_FAILED% time=0 testname=createCityTest "
                << "(CityDistanceTests) message=error name does not match input" 
                << std::endl;     
    }
    
    if(a.getLatitude() != lat)
    {
        std::cout << "a.getLatitude() = '" << a.getLatitude() << "'" << std::endl
                  << "input was '" << lat << "'" << std::endl;
        std::cout << "%TEST_FAILED% time=0 testname=createCityTest "
                << "(CityDistanceTests) message=error latitude does not match input" 
                << std::endl;     
    }
    
    if(a.getLongitude() != lon)
    {
        std::cout << "a.getLongitude() = '" << a.getLongitude() << "'" << std::endl
                  << "input was '" << lon << "'" << std::endl;
        std::cout << "%TEST_FAILED% time=0 testname=createCityTest "
                << "(CityDistanceTests) message=error londitude does not match input" 
                << std::endl;     
    }
    std::cout << "SUCCESS;\t City Successfully Created: '" << a.getName() 
            << "'" << std::endl;
}

void distanceTests() {
    
    std::cout << "DISTANCE TESTS" << std::endl;
    const double tokyo_london_dist      = 9554.72;
    const double newyork_nicosia        = 8778.93;
    const double error_margin           = 90.0;
    
    City a, b;
    a.setName("London");
    a.setLatitude(51.507222);
    a.setLongitude(-0.1275);
    
    b.setName("Tokyo");
    b.setLatitude(35.689506);
    b.setLongitude(139.6917);
    
    double calculated_dist = CityMaths::calcDistance(a,b);
    std::cout << "Calculated london-toky distance : " << calculated_dist
            << std::endl << "\trecorded distance : " << tokyo_london_dist
            <<std::endl;
    if(calculated_dist > tokyo_london_dist + error_margin ||
            calculated_dist < tokyo_london_dist - error_margin){
        std::cout << "%TEST_FAILED% time=0 testname=distanceTests "
                << "(CityDistanceTests) message=error was outside of error range" 
                << std::endl;
        return;
    }
        
    a.setName("New York");
    a.setLatitude(43);
    a.setLongitude(-75);
    
    b.setName("Lefkosia");
    b.setLatitude(35.166667);
    b.setLongitude(33.366667);
    
    calculated_dist = CityMaths::calcDistance(a,b);
    std::cout << "Calculated newyork-nicosia distance : " << calculated_dist
            << std::endl << "\trecorded distance : " << newyork_nicosia
            <<std::endl;
    if(calculated_dist > newyork_nicosia + error_margin ||
            calculated_dist < newyork_nicosia - error_margin){
        std::cout << "%TEST_FAILED% time=0 testname=distanceTests "
                << "(CityDistanceTests) message=error was outside of error range" 
                << std::endl;
        return;
    }
    std::cout << "SUCCESS;\t Results are accurate " << std::endl;
}

void loadingTest()
{
    std::cout << "LOADING TEST" << std::endl;
    const unsigned int num_cities = 100;
    CityLoader::citymap m;
    m = CityLoader::loadFile("cities");
    if(m.size() == 0){
        std::cout << "%TEST_FAILED% time=0 testname=loadingTest "
                << "(CityDistanceTests) message=no cities were loaded" 
                << std::endl;
        return;
    }
    if(m.size() < num_cities){
        std::cout << "%TEST_FAILED% time=0 testname=loadingTest "
                << "(CityDistanceTests) message=not all the cities were loaded" 
                << std::endl;
        return;
    }
    std::cout << "SUCCESS;\t " << m.size() << " cities have been loaded." 
            << std::endl;
}

void testCityDistModel(){
    std::cout << "'CityDistanceModel' TEST" << std::endl;
    CityDistanceModel cdm;
    City c;
    c.setName("london");
    c.setLatitude(12);
    c.setLongitude(13);
    cdm.addCity(c);
    CityLoader::citymap::iterator it = cdm.findCity("london");
    if(it == cdm.end()){
        std::cout << "%TEST_FAILED% time=0 testname=testCityDistModel "
                << "(CityDistanceTests) message=The added city could not be found" 
                << std::endl;
    }
    
    std::cout << "Successfully Added City: " << std::endl;
    cdm.showCityInfo("london");
    
    std::cout << "Removing City:" << std::endl;
    cdm.removeCity(c);
    it = cdm.findCity("london");
    if(it != cdm.end()){
        std::cout << "%TEST_FAILED% time=0 testname=testCityDistModel "
                << "(CityDistanceTests) message=The added city could not be deleted" 
                << std::endl;
    }
    
    std::cout << "Successfully Deleted City " << c.getName() << std::endl;
    
    const double newyork_nicosia        = 8778.93;
    const double error_margin           = 90;
    City a, b;
    a.setName("New York");
    a.setLatitude(43);
    a.setLongitude(-75);
    
    b.setName("Lefkosia");
    b.setLatitude(35.166667);
    b.setLongitude(33.366667);
    
    cdm.addCity(a);
    cdm.addCity(b);
    double dist = cdm.getDistance("New York", "Lefkosia");
    std::cout << "Calculated newyork-nicosia distance : " << dist
            << std::endl << "\trecorded distance : " << newyork_nicosia
            <<std::endl;

    if(dist > newyork_nicosia + error_margin ||
            dist < newyork_nicosia - error_margin){
        std::cout << "%TEST_FAILED% time=0 testname=testCityDistModel "
                << "(CityDistanceTests) message=calculated city distance; error was outside of error range" 
                << std::endl;
        return;
    }
    std::cout << "Successfully calculated city distance" << std::endl;

    std::cout << "Show All Cities: " << std::endl;
    cdm.showAll();
    
    std::cout << "Modifying City Lefkosia" << std::endl;
    cdm.showCityInfo("Lefkosia");
    City q;
    q.setName("Nicosia");
    q.setLongitude(0);
    q.setLatitude(0);
    
    cdm.modifyCity("Lefkosia", q);
    
    std::cout << "Lefkosia is now : ";
    cdm.showCityInfo("Nicosia");
    std::cout << "SUCCESS;" << std::endl;
}

int main(int argc, char** argv) {
    std::cout << "%SUITE_STARTING% CityDistanceTests" 
            << std::endl;
    
    std::cout << "%SUITE_STARTED%" 
            << std::endl;

    std::cout << "%TEST_STARTED% testCityDistModel (CityDistanceTests)" 
            << std::endl;
    
    testCityDistModel();
    
    std::cout << "%TEST_FINISHED% time=0 testCityDistModel (CityDistanceTests)" 
            << std::endl;
    
    std::cout << "%TEST_STARTED% createCityTest (CityDistanceTests)" 
            << std::endl;
    
    createCityTest();
    
    std::cout << "%TEST_FINISHED% time=0 createCityTest (CityDistanceTests)" 
            << std::endl;
    
    std::cout << "%TEST_STARTED% distanceTests (CityDistanceTests)" 
            << std::endl;
    
    distanceTests();
    
    std::cout << "%TEST_FINISHED% time=0 distanceTests (CityDistanceTests)" 
            << std::endl;

    std::cout << "%TEST_STARTED% loadingTest (CityDistanceTests)" 
            << std::endl;
    
    loadingTest();
    
    std::cout << "%TEST_FINISHED% time=0 loadingTest (CityDistanceTests)" 
            << std::endl;

    std::cout << "%SUITE_FINISHED% time=0" 
            << std::endl;

    return (EXIT_SUCCESS);
}

