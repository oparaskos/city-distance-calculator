/* 
 * File:   ErrorStack.cpp
 * Author: oliver
 * 
 * Created on 06 November 2013, 11:06
 */

#include "ErrorStack.h"

std::string ErrorStack::error_t::getMessage()
{
    return message;
}
void ErrorStack::error_t::setMessage(std::string msg)
{
    message = msg;
}
error_code ErrorStack::error_t::getError()
{
    return error;
}
void ErrorStack::error_t::setError(error_code e)
{
    error = e;
}

void ErrorStack::push(error_code c)
{
    error_t e;
    e.setError(c);
    e.setMessage(toString(c));
    
    errorStack.push(e);
}
void ErrorStack::push(error_code c, std::string msg)
{
    error_t e;
    e.setError(c);
    e.setMessage(toString(c, msg));
    
    errorStack.push(e);
}
    
void ErrorStack::push(error_t e)
{
    errorStack.push(e);
}

ErrorStack::error_t ErrorStack::pop()
{
    ErrorStack::error_t e = errorStack.top();
    errorStack.pop();
    return e;
}

ErrorStack::error_t ErrorStack::readLast()
{
    return errorStack.top();
}

size_t ErrorStack::size()
{
    return errorStack.size();
}

void ErrorStack::clear()
{
    while(errorStack.size())
        errorStack.pop();
}

std::string toString(error_code e)
{
	/* convert error code enum to a 
		sensible error string to output */
    switch(e){
    case ERR_ALREADY_EXISTS:
        return "Already exists";
    case ERR_COULD_NOT_RENAME:
        return "Could not rename";
    case ERR_INVALID_OPTION:
        return "Invalid Option";
    case ERR_NO_SUCH_CITY:
        return "No such city";
    case ERR_FILE_ERROR:
        return "Error Opening File";
    case ERR_SYNTAX_ERR:
        return "Syntax Error";
    default:
        return "Undefined Error";
    }
}

std::string toString(error_code e, std::string suffix)
{
	/* Construct the error string appending the 
		suffix (helpful error message) at the end */
    return toString(e).append(" : ").append(suffix).append("\n");
}

std::string toString(error_code e, std::string prefix,
        std::string suffix)
{
	/* Construct the error string appending the 
		suffix at the end and prefix at the start */
    return prefix.append(" : ").append(toString(e, suffix));
}