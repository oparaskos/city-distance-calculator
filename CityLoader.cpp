#include "ErrorStack.h"
#include "CityLoader.h"
#include "Model.h"

void replace(std::string& myString, char find, char replace)
{
    for(std::string::iterator it = myString.begin();
		it != myString.end();
		++it)
	{
		if(*it == find)
			*it=replace;
	}
}
  
bool isEmpty(std::string str)
{
  int retn = 0;
  for (auto i = str.begin(); i != str.end(); ++i){
    retn = retn |  !(isspace(*i));
  }
  return !retn;
}

std::string trim(const std::string& str,
                 const std::string& whitespace)
{
    auto strBegin = str.find_first_not_of(whitespace);
    if (strBegin == std::string::npos)
        return ""; // no content

    auto strEnd = str.find_last_not_of(whitespace);
    auto strRange = strEnd - strBegin + 1;

    return str.substr(strBegin, strRange);
}

namespace CityLoader
{
void readCoordinate(std::string str, double & degrees, double & minutes, double & seconds, char & ch )
{
        const unsigned int BUF_SIZE = 24;
	/* clear all the variables (in case they had some value) */
	degrees	=0-0xdeadbeef;
	minutes	=0;
	seconds	=0;
	
	/* east would do too, just some default that doesnt 
	 * change the sign of the degrees in case there is no direction stated 
	 * explicitly (there may be a sign already instead): */
	ch	='N'; 
	
	char buf[BUF_SIZE];
	unsigned int index = 0;
	unsigned int dmos = 0;//degrees minutes or seconds.
	
	/* clear the buffer */
	memset(buf, 0, BUF_SIZE);
	
	for (std::string::iterator it = str.begin();
                it != str.end(); 
                ++it) {
            
		switch(*it) {
		/* If there's a space then stop reading the 
		 * current number and read the next*/
		case '_' :
		case ' ' :
			dmos ++;
			
			/* clear the buffer */
			memset(buf, 0, BUF_SIZE);
			index = -1; //will be 0 on the next loop round...
			break;
			
		/* read the sign */
		case 'N': case 'E': case 'S': case 'W':
		case 'n': case 'e': case 's': case 'w':
			ch = *it;
			return;
		
		/* if there's no special case then continue reading 
		 * numbers into the buffer for the current unit
		 * while there's still space in the bugger*/
		default:
			if(index < BUF_SIZE){ //check buffer overflow
				buf[index] = *it;
				switch(dmos) {
				case 0: 
					degrees = atof(buf); 
					break; 
				case 1: 
					minutes = atof(buf);
					break; 
				case 2: 
					seconds = atof(buf);
					break; 
				default:
					ErrorStack::GetSingleton().push(error_code::ERR_UNDEFINED,
						"Something Went Wrong");
					/* Something Went Wrong!*/
					return;
				}
			}
			break;
		}
		index ++;
	}
	
	if(degrees == 0-0xdeadbeef){ //if the degrees is a special value then something went wrong..
        ErrorStack::GetSingleton().push(error_code::ERR_SYNTAX_ERR,
            std::string("Could not convert number '")
			.append(str)
			.append("'") );
	}
} /* readCoordinate(...) */

City parseCity(std::string StrCityInfo)
{
	City c;
	/* make it into a stream for easy formatted output */
	std::stringstream StrStrCityInfo(StrCityInfo);
		
	/* get the city name (first token in string of 3)) */
	std::string StrCityName = "";
	StrStrCityInfo >> StrCityName;
        
	/** @todo: Error Handling */
        
	/* Underscores are used to escape spaces
	 * so that streams can be used easily, convert them back */
	replace(StrCityName, '_', ' ');
	
	double dLatitude = 0.0;
	double dLongitude = 0.0;
	
	/** Get the Longitude, and flatten minutes and seconds into degrees */
	{
		//get the Lon
		std::string StrLongidtude = "";
		StrStrCityInfo >> StrLongidtude;
		replace(StrLongidtude, '_', ' ');
		
		double minutes, seconds;
		char dir;
		
		readCoordinate(StrLongidtude, dLongitude, minutes, seconds, dir);		
		CityMaths::flattenDegrees(dLongitude, minutes, seconds, dir);
	}
	
	/** Get the Latitude, and flatten minutes and seconds into degrees */
	{
		//get the Lat
		std::string StrLatitude = "";
		StrStrCityInfo >> StrLatitude;
		replace(StrLatitude, '_', ' ');
				
		double minutes, seconds;
		char dir;
		
		readCoordinate(StrLatitude, dLatitude, minutes, seconds, dir);
		CityMaths::flattenDegrees(dLatitude, minutes, seconds, dir);
	}
	
	/* construct the data into a city */
	c.setLatitude(dLatitude);
	c.setLongitude(dLongitude);
	c.setName(StrCityName);
	return c;
}/* parseCity(...) */

citymap loadFile(std::string const & fileIn)
{
    std::string StrFileToLoad = trim(fileIn, " \t");/*@hack*/
    citymap arrCities;
    std::ifstream inFile(StrFileToLoad, std::ios::in);

    /* If there was an error, print some info about it in the error log and
     * return the empty citymap*/
    if(!inFile.good()){
        ErrorStack::GetSingleton().push(error_code::ERR_FILE_ERROR,
            std::string("'").append(StrFileToLoad).append("' was not good") );
        inFile.close();
        return arrCities;
    }

    /* Read each line in turn (each line contains 3 tokens city name, longitude
     * and latitude) submit the line for converting into a city and then 
     * add the city to the city map */
    do {
        City c;
        
        std::string line;
        getline(inFile, line);	

        c = parseCity(line);
        arrCities[c.getName()] = c;

    } while ( inFile.good() );

    //close the file.
    inFile.close();
    return arrCities;
}/* loadFile(...) */

bool saveFile(std::string const & fileIn)
{
    std::string StrFileToLoad = trim(fileIn, " \t");/*@hack*/
    std::ofstream outFile(StrFileToLoad, std::ios::out);

    /* If there was an error, print some info about it in the error log and
     * return the empty citymap*/
    if(!outFile.good()){
    ErrorStack::GetSingleton().push(error_code::ERR_FILE_ERROR,
            std::string("'").append(StrFileToLoad).append("' was not good") );
            outFile.close();
            return false;
    }

    /* Read each line in turn (each line contains 3 tokens city name, longitude
     * and latitude) submit the line for converting into a city and then 
     * add the city to the city map */
    auto it = Model::GetSingleton().begin();
    while(it != Model::GetSingleton().end()
            && outFile.good())
    {
        std::string name = it->second.getName();
        replace(name, ' ', '_');
        outFile << "\n" 
                << name
                << "\t" << it->second.getLongitude() << "_N"
                << "\t" << it->second.getLatitude() << "_E";
        ++it;
    }

    //close the file.
    outFile.close();
    return true;
}/* loadFile(...) */

}/* namespace CityLoader */
