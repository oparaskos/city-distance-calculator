/**
 * @author Emogene Atkinson - 12015483
 * @author Oliver Paraskos - 13074673
 * 
 * This file will do the maths needed for calculating the distance between 2
 * places.
 */

#ifndef CITYMATHS_H
#define	CITYMATHS_H

#include <cmath>
#include "City.h"
const long double PI = 3.14159265358979323846264338327950288419716939937510582f;

namespace CityMaths
{
    /**
     * @brief degrees to radians converter.
     * @return radians.
     */
    inline double radians(double a);
    
    /**
     * @brief radians to degrees converter.
     * @return degrees.
     */
    inline double degrees(double a);
    
    /**
     * @brief gets the distance as the crow flies in kilometers between 2 cities
     * ...
     * @return distance in km.
     */
    double calcDistance(City a, City b); 
    ///TODO:: Split this up into seperate functions? eg toKilometers() getAngularDistance()...
    
    /**
     * @brief turns a geographical coordinate from Degrees, Minutes, Seconds, 
     * and N/S/E/W and turns it into a real number with just degrees.
     * @param &degrees in/out degrees from the aggregate degrees, minutes, 
     * seconds and direction
     * @param minutes minutes from the coordinate or 0
     * @param seconds seconds from the coordinate or 0
     * @param direction sign of the angle
     * 
     * @return (copy of) degrees of the angle
     */
    double flattenDegrees(double &degrees, double minutes, 
            double seconds, char direction); 
    ///TODO:: flattenDegrees(...) return should be const?
}

#endif	/* CITYMATHS_H */

