/* 
 * File:   custom_hashtable.h
 * @author Oliver Paraskos - 13074673
 *
 * Created on 20 November 2013, 11:02
 */

#ifndef CUSTOM_HASHTABLE_H
#define	CUSTOM_HASHTABLE_H

#include <utility>
#include <iterator>

template <class U>
friend bool operator==(const U&, const U&);

template <class U>
friend bool operator!=(const U&, const U&); 

template<typename K, typename V>
class custom_hashtable {
public:
    custom_hashtable(){}
    custom_hashtable(const custom_hashtable& orig){}
    ~custom_hashtable(){}
    
    class iterator : public std::iterator<std::input_iterator_tag, std::pair<K,V>>
    {
        std::pair<K,V>* p;
    public:
        iterator(std::pair<K,V> x) : p(x) {}
        iterator(const iterator& copy) : p(copy.p) {}
        iterator& operator++() {++p; return *this;}
        iterator operator++(int) {iterator tmp(*this); ++p; return tmp;}
        bool operator==(const iterator& rhs) {return p==rhs.p;}
        bool operator!=(const iterator& rhs) {return p!=rhs.p;}
        std::pair<K,V>& operator*(){return *p;}        
    };
    
    iterator begin(){/*return iterator to pointer to start */};
    iterator end(){/*return iterator to pointer to end */};
    iterator at(K const & key){return begin();/* search and find K and return iterator to it */}
    iterator operator[](K const & key){return begin();/* search and find K and return iterator to it*/};
    
    unsigned int size();
    unsigned int hash(K const & key);
    
    void quicksort();
    void binarysearch(K const & key);
    
    void findvalue(V const & value);
    
    
private:
    
};

#endif	/* CUSTOM_HASHTABLE_H */

