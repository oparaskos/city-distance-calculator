/* View Interface
 * Author:
 * Emogene Atkinson
 */

#include "Singleton.hpp"

class View : public Singleton<View>
{
public:
    View(): Singleton<View>() {}
    virtual ~View(){}
    virtual void printMessage(std::string message) = 0;
};