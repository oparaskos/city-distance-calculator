/* 
 * File:   Hashable.hpp
 * Author: Oliver Paraskos - 13074673
 * 
 * Hacky generic hashable class.
 * Uses F/N/V-1a as a hashing algorithm.
 * Offset Basis and Prime are from wikipedia.
 *
 * Created on 20 November 2013, 21:01
 */

#ifndef HASH_HPP
#define	HASH_HPP

template <class C>
class Hash
{
	const static int fnvOffsetBasis = 0xcbf29ce484222325;
	const static int fnvPrime	 = 0x100000001b3;
public:
        Hash()
        : mfirst(0) {}
	
	Hash(C toHash)
        : mfirst(0),
          msecond(toHash) {
            hash();
        }
	
	int hash()
	{		
		/* FNV1a:			
		 * hash = FNV_offset_basis
		 * for each octet_of_data to be hashed
		 *	hash = hash XOR octet_of_data
		 *	hash = hash * FNV_prime
		 * return hash
		 */
		
		mfirst = fnvOffsetBasis;
		for(int i =0; i < msecond.size(); ++i){
			mfirst = mfirst ^ (char)msecond[i];
			mfirst = mfirst * fnvPrime;
		}
		return mfirst;
	}
        
        void set(C nsecond){
            msecond = nsecond;
            hash();
        }
        
        int first(){return hash();}
	
        C& second(){return msecond;}
	
	inline friend bool operator <(Hash<C>& lhs, Hash<C>& rhs)
	{
		return lhs.first() < rhs.first();
	}

	inline friend bool operator >(Hash<C>& lhs, Hash<C>& rhs)
	{
		return lhs.first() > rhs.first();
	}
	
	inline friend bool operator ==(Hash<C>& lhs, Hash<C>& rhs)
	{
		return lhs.first() == rhs.first();
	}
	
	
private:
	int mfirst;
	C msecond;
        
};
#endif	/* HASHABLE_HPP */

