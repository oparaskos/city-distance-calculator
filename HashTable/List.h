/* 
 * File:   List.h
 * Author: Paraskos
 *
 * Created on 21 November 2013, 16:56
 */

#ifndef LIST_H
#define	LIST_H

#include <algorithm>
#include <string.h>
#include <thread>
#include <iostream>
#include <cassert>

template<class C> int partition(C[], int, int);
template<class C> void squicksort(C[], int, int);
template<class C> int binarysearch(C[], C, int, int);

int centerpoint(int min, int max){return min + (max-min)/2;}

template <class C>
class List {
	const int RESIZE_AMOUNT = 5;
public:
	List() : mNumElems(0), mArray(0), mSize(0) {}
        
	void resize(std::size_t newSize){
		C* temp = new C[mNumElems];
		if(mNumElems)
			memcpy(temp, mArray, mNumElems*sizeof(C));
                
		delete[] mArray;
		mArray = new C[newSize];
                mSize = newSize;
                
		if(mNumElems)
			memcpy(mArray, temp, mNumElems*sizeof(C));
		delete[] temp;
	}
	
	void add(C const & elem){
		if(mSize < mNumElems + 1){
			resize(mSize + RESIZE_AMOUNT);
		}
		mArray[mNumElems] = elem;
		++mNumElems;
	}
        
        C at(int index){
            assert(index < mNumElems);
            return mArray[index];
        }
        
	void quicksort(){
		squicksort(mArray, 0, mNumElems-1);
	}
	
	int find(C elem){
		return binarysearch(mArray, elem, 0, mNumElems-1);
	}
	
        std::size_t size(){return mNumElems;}
        
private:
	C* mArray;
	std::size_t mNumElems;
	std::size_t mSize;
};

template<class C>
void squicksort(C array[], int L, int R){
	int middle;
	if(L < R){
		middle = partition(array, L, R);
		squicksort(array, L, middle );
		squicksort(array, middle+1, R);
	}
	return;
}

template<class C>
int partition(C array[], int L, int R)
{
	C x = array[L];
	C temp;
	int i = L -1;
	int j = R + 1;
	
	do{
		do{ i++; }while( array[i] < x );
		do{ j--; }while( x < array[j] );
		
		if( i < j ) {
			temp = array[i]; 
			array[i] = array[j]; 
			array[j] = temp;
		}
	}while(i < j);
	return j;
}

template<class C>
int binarysearch(C array[], C target, int L, int R)
{
	while (R >= L) {
		int center = centerpoint(L, R);
		
		if(array[center] == target) { //found it!
			return center; 
		
		} else if (array[center] < target) {
			L = center + 1;
			
		} else {
			R = center - 1;
			
		}
	}
	return -1;
}

#endif	/* LIST_H */

