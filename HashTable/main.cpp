/* 
 * File:   main.cpp
 * Author: Paraskos
 *
 * Created on 20 November 2013, 21:00
 */

#include <cstdlib>
#include <string>
#include <ios>
#include <iostream>
#include <vector>
#include "HashList.h"

int amain(int argc, char** argv)
{
	std::vector<std::string> helloWorld = {
    "Fred",
	"Wilma", 
    "Barney", 
    "Homer", 
    "Marge",
    "acolyte",
    "acolytes",
    "aconite",
    "aconites",
    "aconitic",
    "aconitum",
    "acorn",
    "acorns",
    "acoustic",
    "acquaint",
    "acquest",
    "acquests",
    "acquire",
    "acquired",
    "acquirer",
    "acquires",
    "acquit",
    "acquits",
    "acrasia",
    "acrasias",
    "acrasins",
    "acre",
    "acreage",
    "acreages",
    "acred",
    "acres",
    "acrider",
    "acridest",
    "acridine",
    "acridity",
    "acridly",
    "acrimony",
    "acrobat",
    "acrobats",
    "acrodont",
    "acrogen",
    "acrogens",
    "acrolect",
    "acrolein",
    "acrolith",
    "acromia",
    "acromial",
    "acrolith",
    "acromia",
    "acromial",
    "acrolith",
    "acromia",
    "acromial",
    "acrolith",
    "acromia",
    "acromial",
    "acromion",
    "acromion",
    "acromion",
    "acromion",
    "acromion",
    "acronic",
    "acronic",
    "acronic",
    "acronic",
    "acronic",
    "acronic"};
	
    HashList<std::string> myList;
    myList.resize( helloWorld.size() );
    
    for(int i =0; i < helloWorld.size(); ++i){
        Hash<std::string> q;
        q.set(helloWorld.at(i));
        myList.add(q);
    }
    
	myList.quicksort();
	
    for(int i =0; i < myList.size(); ++i){
        std::cout << myList.at(i).first() << " : " << myList.at(i).second() << std::endl;
    }
	
	for(int i =0; i < helloWorld.size(); ++i){
        Hash<std::string> q;
        q.set(helloWorld.at(i));
        if(myList.find(q) >=0)std::cout << "found." << std::endl;
    }
	
	/*
	std::vector<int> myVec = {1,6,2,5,7,3,12,1,6,47,2,3,53,48,3,23,999,8,34,0};
	List<int> myList;
	for(int i = 0;i < myVec.size(); i++){
		myList.add((int) myVec.at(i));
	}
	
	for(int i =0; i < myList.size(); ++i){
        std::cout << myList.at(i) << std::endl;
    }
	
	std::cout << "..." << std::endl;
	myList.quicksort();
	
	for(int i =0; i < myList.size(); ++i){
        std::cout << myList.at(i) << std::endl;
    }
	std::cout << "..." << std::endl;
	for(int i =0; i < myVec.size(); ++i){
        std::cout << myList.find(myVec.at(i)) << std::endl;
    }
	 */
	return 0;
}
typedef HashPair<std::string, std::string> StrStrPair;
int main(int argc,char** argv){
	HashMap<std::string, std::string> hashMap;
	hashMap.add(StrStrPair("hello", "KITTY"));
	hashMap.add(StrStrPair("sherlock", "HOLMES"));
	hashMap.add(StrStrPair("george", "FOREMAN"));
	hashMap.add(StrStrPair("ping", "PONG"));
	
	/*std::cout << hashMap["hello"].first.second() << " " << hashMap["hello"].second << std::endl;
	std::cout << hashMap["george"].first.second() << " " << hashMap["george"].second << std::endl;
	std::cout << hashMap["sherlock"].first.second() << " " << hashMap["sherlock"].second << std::endl;
	std::cout << hashMap["ping"].first.second() << " " << hashMap["ping"].second << std::endl;
	 */
	for(int j = 0; j < hashMap.size(); j++){
		std::cout << hashMap.at(j).first.second();
		int i = hashMap.find(hashMap.at(j).first.second());
		if(i >= 0){
			std::cout << " " << hashMap.at(i).second << std::endl;
		}else{
			std::cout << " Couldn't Find" << std::endl;
			HashPair<std::string,std::string> hp;
			hp.first.set(hashMap.at(j).first.second());
			int i = hashMap.find(hp);
			if(i >= 0 ){
				return hashMap.at(i);
			}
		}
	}
}