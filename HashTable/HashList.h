/* 
 * File:   HashList.h
 * Author: oliver
 *
 * Created on 22 November 2013, 13:27
 */

#ifndef HASHLIST_H
#define	HASHLIST_H

#include "Hash.hpp"
#include "List.h"

template <class C>
class HashList : public List< Hash<C> > 
{
	//C& operator[](C toHash){ return at(0);} //@todo:
};

template<class A, class B>
class HashPair{
public:
	HashPair(){}
	HashPair(A a, B b) : first(a), second(b){}
	Hash<A> first;
	B second;
	
	inline friend bool operator <(HashPair<A,B>& lhs, HashPair<A,B>& rhs) {
		return lhs.first < rhs.first;
	}

	inline friend bool operator >(HashPair<A,B>& lhs, HashPair<A,B>& rhs) {
		return lhs.first > rhs.first;
	}
	
	inline friend bool operator ==(HashPair<A,B>& lhs, HashPair<A,B>& rhs) {
		return lhs.first == rhs.first;
	}
};

template <class A, class B>
class HashMap : public List< HashPair< A, B> > 
{
public:
	HashPair<A, B> operator[](A toHash){
		HashPair<A,B> hp;
		hp.first.set(toHash);
		int i = this->find(hp);
		if(i >= 0 ){
			return this->at(i);
		}
	}
	int find(A toHash){
		HashPair<A,B> hp;
		hp.first.set(toHash);
		return this->List<HashPair<A,B>>::find(hp);
	}
};

#endif	/* HASHLIST_H */

