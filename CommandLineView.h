/**
 * File: CommandLineView.h
 * @author Emogene Atkinson
 */
#ifndef COMMANDLINEVIEW_H
#define COMMANDLINEVIEW_H

#include "View.h"
#include <string>
#include <cstdio>
 
 class CommandLineView : public View
{
public:
    CommandLineView(): View(){}
    ~CommandLineView(){}
    
    void printMessage(std::string message);
    
 };

/** Easy output to view */
View & operator<<(View& o, const std::string & message);
/** Easy output (real numbers) to view */
View & operator<<(View& o, const double & d);
/** Easy output (ints) to view */
View & operator<<(View& o, const int & d);
View & operator<<(View& o, const unsigned int & d);
#endif