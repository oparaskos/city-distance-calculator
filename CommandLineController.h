/**
 * File: CommandLineController.h
 * @author Katie Jarvis
 */
#ifndef COMMANDLINECONTROLLER_H
#define COMMANDLINECONTROLLER_H
 
#include "Controller.h"
#include <string>
 
class CommandLineController : public Controller
{
public:
    CommandLineController(): Controller() {}
    ~CommandLineController(){}
    void control();
};

#endif