/*
 * Authors: 
 * Emogene Atkinson - 12015483
 * Oliver Paraskos - 13074673
 * 
 * This file will do the maths needed for calculating the distance between 2
 * places.
 */
#include "CityMaths.h"

namespace CityMaths
{
    const static double EARTH_RADIUS = 6371.0;
    
    inline double radians(double a){
        return a * PI / 180;
    }

    inline double degrees(double a){
        return a * 180 / PI;
    }

    double calcDistance(City a, City b){
        double cosAngularDistance = 0;  //cosine of the angular distance in rads
        double angularDistance    = 0;  //angular distance in degrees
        double kilometerDistance  = 0;  //geographical distance in km

        double aLat = radians(a.getLatitude());
        double bLat = radians(b.getLatitude());

        double aLon = radians(a.getLongitude());
        double bLon = radians(b.getLongitude());

        /* cos(d) = sin(alat) sin(blat) + cos(alat) cos(blat) cos(along - blong) */
        cosAngularDistance = 
                (
                    (sin(aLat) * sin(bLat)) +
                    (cos(aLat) * cos(bLat) * cos(aLon - bLon))
                );
        
        /* d = acos(cos(d)) */
        angularDistance = degrees(acos(cosAngularDistance));
        
        /* 6371 pi d  */
        /* __________ */
        /*     180    */
        kilometerDistance = (EARTH_RADIUS * PI * angularDistance)/180.0;
        return kilometerDistance;
    } /* calcDistance() */


    double flattenDegrees(double & degrees, double minutes, 
            double seconds, char direction)
    {
            /* 60 seconds per minute: */
            minutes += (seconds/60);	
            /* 60 minutes per degree: */
            degrees += (minutes/60);

            /* If the direction is South or East then the degrees are negative*/
            switch(direction)
            {
            case 's':
            case 'S':
                    degrees *= -1;
                    break;
            case 'w':
            case 'W':
                    degrees *= -1;
                    break;
            }
            return degrees;
    } /* flattenDegrees() */
}