/* City Distance Model Class 
 * This file will contain functions to add, remove, load, modify, save and display cities.
 * Author:
 * Emogene Atkinson
 * Oliver Paraskos
 * Katie Jarvis
 */
 
#include "CityDistanceModel.h"
#include "CommandLineView.h"

#define GETVIEW CommandLineView::GetSingleton()

#include "ErrorStack.h"
bool CityDistanceModel::addCity(City city)
{
	auto c = findCity(city.getName());
	if(c != Model::GetSingleton().end()){
		ErrorStack::GetSingleton().push(error_code::ERR_ALREADY_EXISTS, 
			std::string("'")
			.append(city.getName())
			.append("' already exists in the database") );
		return false;
	}
	
	unsigned int old_num_cities = mCityMap.size();
	std::pair<std::string, City> inspair(city.getName(), city);
	
	mCityMap.insert(inspair);
	
	if(old_num_cities == mCityMap.size() - 1)
		return true;
	return false;
}

bool CityDistanceModel::removeCity(City city)
{
	unsigned int old_num_cities = mCityMap.size();
        
	if(findCity(city.getName()) == mCityMap.end())
		return false;
        
	mCityMap.erase(city.getName());
        
	if(old_num_cities == mCityMap.size() + 1)
		return true;
        
	return false;
}

bool CityDistanceModel::modifyCity(std::string lookup, City newcity)
{
    City oldcity = findCity(lookup)->second;
    if(!newcity.getName().compare(""))
        newcity.setName(oldcity.getName());
    
    if(!newcity.getLatitude())
        newcity.setLatitude(oldcity.getLatitude());
    
    if(!newcity.getLongitude())
        newcity.setLongitude(oldcity.getLongitude());
    
    removeCity(oldcity);
    addCity(newcity);
    return true;
}

bool CityDistanceModel::loadCities(std::string path ="cities")
{
	CityLoader::citymap new_cities;
	new_cities = CityLoader::loadFile(path);
	if(!new_cities.size()){
		return false;
	}
	mCityMap.insert(new_cities.begin(), new_cities.end());
	return true;
}

bool CityDistanceModel::saveCities(std::string path ="cities")
{
    CityLoader::saveFile(path);
    return false;
}

CityLoader::citymap::iterator CityDistanceModel::findCity(string cityName)
{
    replace(cityName, '_', ' ');           //Use underscore as a space-escaper..
    return mCityMap.find(cityName);
}

double CityDistanceModel::getDistance(string cityA, string cityB)
{
    CityLoader::citymap::iterator a, b;
    a = findCity(cityA);
    b = findCity(cityB);
	
	/*Check that the citys are loaded in the database */
	{
		bool error = false;
		
		if (a == end()) {
			ErrorStack::GetSingleton().push(error_code::ERR_NO_SUCH_CITY, 
				std::string("Could not find '").append(cityA).append("'") );
			error=true;
		}
		
		if (b == end()) {
			ErrorStack::GetSingleton().push(error_code::ERR_NO_SUCH_CITY, 
				std::string("Could not find '").append(cityB).append("'") );
			error=true;
		}
		
		if(error) return -2;
	}
	
	
    if (a != mCityMap.end() 
            && b != mCityMap.end()) {         
       return CityMaths::calcDistance(a->second, b->second);
    }
	
    return -1.0; //negative values indicate error code.
}

void CityDistanceModel::showCityInfo(string cityName)
{
    CityLoader::citymap::iterator it = findCity(cityName);
    GETVIEW   << it->second.getName() << "\n"
              << "\t" << it->second.getLongitude()
              << "N " << it->second.getLatitude() << "E \n";
}

void CityDistanceModel::showAll()
{
    CityLoader::citymap::iterator it = mCityMap.begin();
    for(; it != mCityMap.end(); ++it){
        GETVIEW << it->second.getName() << "\n"
                << "\t" << it->second.getLongitude()
                << "N " << it->second.getLatitude() << "E "
                << "\n";
    }
}

CityLoader::citymap::iterator CityDistanceModel::end()
{
    return mCityMap.end();
}

CityLoader::citymap::iterator CityDistanceModel::begin()
{
    return mCityMap.begin();
}

unsigned int CityDistanceModel::size()
{
    return mCityMap.size();
}

