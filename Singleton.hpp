/**
 * File:   Singleton.h
 * @author Oliver Paraskos - 13074673
 * 
 * A Template Singleton base class that automatically contains a static 
 * Singleton<T> pointer, and allows chopping& changing of that pointer at 
 * runtime. The pointer is deleted in the singleton classes destructor.
 * 
 * Based on Scott Bilas' templatised singleton base class
 * in Game Programming Gems.
 * Created on 16 October 2013, 12:24
 */

#ifndef SINGLETON_H
#define SINGLETON_H

#ifndef CPP11
#define nullptr 0
#endif

#include "CustomAssert.h"

/**
 * An semi-automatic template singleton base class.
 * @detail The class automatically contains a static 
 * Singleton<T> pointer.
 * Allows chopping& changing of that pointer at 
 * runtime.
 * The pointer is deleted in the singleton classes destructor.
 */
template <typename T> class Singleton {
public:
    Singleton(){}
    virtual ~Singleton()
    {}
public:
    /**
     * return a reference to the singleton instance.
     * an assertion checks for nullptr before returning.
     */
    static T& GetSingleton()
    {
        __cassert(ms_pSingleton && 
                "GetSingleton(): Singleton not yet instantiated!");
        return (*ms_pSingleton);
    }

    /**
     * return a pointer to the singleton instance.
     * an assertion checks for nullptr before returning.
     */
    static T* GetSingletonPtr()
    {
        __cassert(ms_pSingleton && 
                "GetSingletonPtr(): Singleton not yet instantiated!");
        return ms_pSingleton;
    }

    /**
     * set the static singleton pointer to an instance of the derivative
     * or derivative derivative Singleton class.
     */
    static void SetSingletonPtr(T* pSingleton)
    {
        ms_pSingleton = pSingleton;
    }
    
protected:
    //! the static pointer to a singleton implementation.
    static T* ms_pSingleton;
};
//initialise the ms_Singleton to point to nothing otherwise it wont be found..
template<typename T> T* Singleton<T>::ms_pSingleton = nullptr;

#endif    /* SINGLETON_H */