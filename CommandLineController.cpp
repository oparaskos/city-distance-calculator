/**
 * File: CommandLineController.cpp
 * @author Katie Jarvis
 * @author Emogene Atkinson
 * @author Oliver Paraskos - 13074673
 */

#include "CommandLineController.h"
#include "CommandLineView.h"
#include "CityDistanceModel.h"
#include "CityLoader.h"
#include "ErrorStack.h"
#include <sstream>
#include <regex>

#define GETVIEW CommandLineView::GetSingleton()

void displayHelp(){
    GETVIEW << "'new' create a new city\n"
         << "'delete' delete a city\n"
         << "'edit' edit a city\n"
         << "'distance' calculate distances\n"
         << "'list' list loaded cities\n"
         << "'load' load a list of cities\n"
         << "'save' save all loaded cities to disk\n"
         << "'sort' sort the list of cities\n"
         << "'help' show this page\n"
         << "'where' print city's coordinates\n"
         << "'quit' close the program\n";
}

void displayDetailedHelp(std::string input)
{   
    input = trim(input);
    if (input.compare("new") == 0) {
        GETVIEW << "'new' create a new city\n"
                << "Syntax:\n\tnew [name of city]\n";
        
    } else if (input.compare("delete") == 0) {
        GETVIEW << "'delete' delete a city\n"
                << "Syntax:\n\tdelete [name of city]\n";
        
    } else if (input.compare("edit") == 0) {
        GETVIEW << "'edit' edit a city\n"
                << "Syntax:\n\tedit [name of city]\n";
        
    } else if (input.compare("distance") == 0) {
        GETVIEW << "'distance' calculate distances\n"
                << "Syntax:\n\tdistance [name of city1] [name of city2]\n";
        
    } else if (!input.compare("list")) {
        GETVIEW << "'list' list loaded cities\n"
                << "Syntax:\n\tlist\n";
    
    }else if (!input.compare("load")) {
        GETVIEW << "'load' load a list of cities\n"
                << "Syntax:\n\tload [path to cities list]\n";
		
    } else if (!input.compare("save")) {
        GETVIEW << "'save' save all loaded cities to disk\n"
                << "Syntax:\n\tsave [path to cities list]\n";
        
    } else if (!input.compare("sort")) {
        GETVIEW << "'sort' sort the list of cities\n"
                << "Syntax:\n\tsort\n";
        
    } else if (!input.compare("help")) {
        GETVIEW << "'help' show help on a specific instruction,\n"
                << " or a list of instructions\n"
                << "Syntax:\n\thelp display help information"
                << "\n\thelp [command] display help on a specific command\n";;
        
    } else if (!input.compare("where")) {
        GETVIEW << "'where' print city's coordinates\n"
                << "Syntax:\n\twhere [city name]\n";
        
    } else if (!input.compare("quit")) {
        GETVIEW << "'quit' close the program\n"
                << "Syntax:\n\tquit\n";
        
    } else {
        ErrorStack::GetSingleton().push(error_code::ERR_INVALID_OPTION,
                string("No Such Command, or Help Page '")
		.append(input)
		.append("'"));
    }
} 



void cmdnew(std::stringstream & params)// create a new city\n"
{
    /* check that there is exactly 3 parameters*/
    std::string x, y, z;
    if(!(params >> x >> y >> z)) {
        ErrorStack::GetSingleton().push(error_code::ERR_INVALID_OPTION,
                "Insufficient Parameters");
        return;
    }
    
    std::string strparams = x;
    strparams.append(" ").append(y);
    strparams.append(" ").append(z);
    
    //load that information into a City object
    City c = CityLoader::parseCity(strparams);
    GETVIEW << "Created '" << c.getName() << "'\n";

    //add that city to the database.
    if(!Model::GetSingleton().addCity(c))
        return;
    
    GETVIEW << "'" << c.getName() << "' has been added\n";
    GETVIEW << Model::GetSingleton().size() << " cities are now loaded\n";
}

void cmddelete(std::stringstream & params)// delete a city\n"
{
	//get the name of the city to remove
	std::string city;
	if(!(params >> city)) { //If there is insufficient parameters
		ErrorStack::GetSingleton().push(error_code::ERR_INVALID_OPTION,
			"Insufficient Parameters");
		return;
	}

	//use an iterator to keep track of the city.
	CityLoader::citymap::iterator c;

	//find the city
	c = Model::GetSingleton().findCity(city);
	if(c == Model::GetSingleton().end()){
		ErrorStack::GetSingleton().push(error_code::ERR_NO_SUCH_CITY, 
			std::string("No Such City '")
			.append(city)
			.append("'") );
		return;
	}
    GETVIEW << "found city '" << city << "'\n";

	//remove city from map
	Model::GetSingleton().removeCity(c->second);
    GETVIEW << "'" << city << "' has been deleted\n";
    GETVIEW << Model::GetSingleton().size() << " cities are now loaded\n";
}

void cmdedit(std::stringstream & params)// edit a city\n"
{
	//get the name of the city to edit
	std::string city;
	if(!(params >> city)) { //If there is insufficient parameters
		ErrorStack::GetSingleton().push(error_code::ERR_INVALID_OPTION,
			"Insufficient Parameters");
		return;
	}
    CityLoader::citymap::iterator c;
    c = Model::GetSingleton().findCity(city);	
	//if the city has been found.
    if(c != Model::GetSingleton().end()){
        GETVIEW << "found '" << city << "'\n";
        GETVIEW	<< "1    Edit Name\n"
                << "2    Edit Coordinates\n"
                //<< "3    Edit Whole City\n"
				<< "selection [1-2] > ";
        std::string selection;
        std::cin >> selection;
        
        if(!selection.compare("1")){
            GETVIEW << "enter a new name> ";
            std::string newName;
            std::cin >> newName;

            if(Model::GetSingleton().findCity(newName) != Model::GetSingleton().end()){
                    ErrorStack::GetSingleton().push(error_code::ERR_ALREADY_EXISTS, 
                            std::string("City '")
                            .append(newName)
                            .append("' Already Exists") );
                    return;
            }

            City nc;
            nc.setName(newName);
            nc.setLongitude(c->second.getLongitude());
            nc.setLatitude(c->second.getLatitude());

            Model::GetSingleton().modifyCity(city, nc);
            
        }else if(!selection.compare("2")){
            GETVIEW << "enter a new coordinate> ";
			double dLongitude, dLatitude;
			
			City nc;
			nc.setName(c->second.getName());
			
			/** Get the Longitude, and flatten minutes and seconds into degrees */
			{
				//get the Lon
				std::string StrLongidtude = "";
				std::cin >> StrLongidtude;
				replace(StrLongidtude, '_', ' ');

				double minutes, seconds;
				char dir;

				CityLoader::readCoordinate(StrLongidtude, dLongitude, minutes, seconds, dir);		
				CityMaths::flattenDegrees(dLongitude, minutes, seconds, dir);
			}

			/** Get the Latitude, and flatten minutes and seconds into degrees */
			{
				//get the Lat
				std::string StrLatitude = "";
				std::cin >> StrLatitude;
				replace(StrLatitude, '_', ' ');

				double minutes, seconds;
				char dir;

				CityLoader::readCoordinate(StrLatitude, dLatitude, minutes, seconds, dir);
				CityMaths::flattenDegrees(dLatitude, minutes, seconds, dir);
			}
						
			if(dLongitude == 0-0xdeadbeef || dLatitude == 0-0xdeadbeef){
				ErrorStack::GetSingleton().push(error_code::ERR_SYNTAX_ERR, 
					"Incorrect Coordinates " );
				return;
			}
			
			
			nc.setLongitude(dLongitude);
			nc.setLatitude(dLatitude);
			
			Model::GetSingleton().modifyCity(city, nc);
            
        }else if(!selection.compare("3")){
            /* check that there is exactly 3 parameters*/
			std::string x, y, z;
			std::cin >> x >> y >> z;

			std::string strparams = x;
			strparams.append(" ").append(y);
			strparams.append(" ").append(z);

			//load that information into a City object
			City c = CityLoader::parseCity(strparams);
			GETVIEW << "Created '" << c.getName() << "'\n";

			//add that city to the database.
			if(!Model::GetSingleton().addCity(c))
				return;
            
			if(Model::GetSingleton().findCity(city) != Model::GetSingleton().end()){
				ErrorStack::GetSingleton().push(error_code::ERR_ALREADY_EXISTS, 
					std::string("City '")
					.append(city)
					.append("' Already Exists") );
				return;
			}
            
        }else{
            ErrorStack::GetSingleton().push(error_code::ERR_INVALID_OPTION,
                "Invalid Selection");
        }
        
    }else{
        ErrorStack::GetSingleton().push(error_code::ERR_NO_SUCH_CITY,
            std::string("No Such City '")
            .append(city)
            .append("'") );
    }
}
void cmddistance(std::stringstream & params)// calculate distances\n"
{
    //name of city to remove
    std::string citya, cityb;
    
    /* check if the string is non-empty */
    if(!(params >> citya >> cityb)) {
        ErrorStack::GetSingleton().push(error_code::ERR_INVALID_OPTION,
                "Invalid Number Of Parameters");
        return;
    }
    
    //removing city from db
    double distance = Model::GetSingleton().getDistance(citya, cityb);
    if(distance >= 0){
        GETVIEW << "the distance from ‘" << citya << "’ to ‘" << cityb
                << "’ is " << distance << " kilometers (" 
                << distance * 0.621371192 << " miles) \n";
    }else{
        ErrorStack::GetSingleton().push(error_code::ERR_UNDEFINED,
                "Error Calculating Distance (Returned A  Negative Number)");
    }
}
void cmdlist(std::stringstream & params)// list loaded cities\n"
{
	if(Model::GetSingleton().size() == 0){
        ErrorStack::GetSingleton().push(error_code::ERR_NO_SUCH_CITY,
                "no cities are loaded.");
		return;
	}
    Model::GetSingleton().showAll();
}
void cmdsort(std::stringstream & params)// sort the list of cities\n"
{
	ErrorStack::GetSingleton().push(error_code::ERR_UNDEFINED,
			"a hash map does not support sorting, \n\
it is automatically sorted based on a hash of the key.");
}
void cmdwhere(std::stringstream & params)// print city's coordinates\n"
{
	/* check if the string is non-empty */
    if(isEmpty(params.str())) {
        ErrorStack::GetSingleton().push(error_code::ERR_INVALID_OPTION,
                "Invalid Number Of Parameters");
        return;
    }
	
    std::string city;
    params >> city;
	
	/* if the city exists in the database. */
    if (Model::GetSingleton().findCity(city) != Model::GetSingleton().end()) {
		Model::GetSingleton().showCityInfo(city);
	}else{
		ErrorStack::GetSingleton().push(error_code::ERR_NO_SUCH_CITY, 
			std::string("Could not find '").append(city).append("'") );
		return;
	}
}

void cmdhelp(std::stringstream & params)
{	
    if(isEmpty(params.str())) {
        displayHelp();
    }else{
        displayDetailedHelp(params.str());
    }
}

void cmdload(std::stringstream & params)
{
	if(Model::GetSingleton().loadCities(params.str()))
        GETVIEW << Model::GetSingleton().size() << " cities are now loaded\n";
}

void cmdsave(std::stringstream & params) 
{
	Model::GetSingleton().saveCities(params.str());
}

void CommandLineController::control()
{
	/* Welcome Message */
    GETVIEW << "== City Distance Calculator ==\n"
            << "Autoloading 'cities'\n"
            << "Help Page:\n";
    displayHelp();
	/* Keep asking for commands until the command is "quit" */
    std::string command;
    
    /* Autoload Cities */
    Model::GetSingleton().loadCities("cities");
    
    do{
		/* output prompt and get input */
	GETVIEW << " > ";
        std::cin >> command;
		
		/* put the parameters into a 
			stringstream for easy access */
        std::string p;
        getline(std::cin, p);
        std::stringstream params;
        params.str(p);

		/* call the appropriate function for each command,
			could be stored in a hash/function pointer lookup table */
        if (command.compare("new") == 0) {
            cmdnew(params);
            
        } else if (command.compare("delete") == 0) {
            cmddelete(params);
            
        } else if (command.compare("edit") == 0) {
            cmdedit(params);

        } else if (command.compare("distance") == 0) {
            cmddistance(params);

        } else if (command.compare("list") == 0) {
            cmdlist(params);
			
        } else if (command.compare("load") == 0) {
            cmdload(params);
			
        } else if (command.compare("save") == 0) {
            cmdsave(params);

        } else if (command.compare("sort") == 0) {
            cmdsort(params);

        } else if (command.compare("help") == 0) {
            cmdhelp(params);

        } else if (command.compare("where") == 0) {
            cmdwhere(params);

        } else if (command.compare("quit") == 0) {
            GETVIEW << "Goodbye!\n";

        } else {
            ErrorStack::GetSingleton().push(error_code::ERR_INVALID_OPTION, 
                std::string("No Such Command '")
                .append(command)
                .append("'"));
        }
        
    } while (command.compare("quit"));
    
    GETVIEW << "Autosaving to cities.autosave\n";
    Model::GetSingleton().saveCities("cities.autosave");
    return;
}