/* 
 * Authors:
 *  Emogene Atkinson - 12015483
 *  Katie Jarvis - 12021767
 * 
 * This class represents a city with a name and a 
 * position in latitude and longitude.
 */
#ifndef CITY_H
#define CITY_H

#include <string>
using std::string;

class City {
private:
    /* Latitude / Longitude geographic coordinates*/
    double longitude;
    double latitude;

    /* Human-Readable Name of the city */
    string name;
public:
    /**! getName()
     * \brief get the name of the city eg 'london'.
     * \return the name of the city
     */
    string getName();

    /**! setName(string)
     * \brief set the name of the city.
     * \param name the new name of the city
     */
    void setName(string name);

    /**! getLongitude()
     * \brief get the longitude of the city.
     * \detail The longitude is purely in degrees no minutes or seconds
     * it has a sign (+/-) instead of N/S/E/W.
     * \return longitude of the position of the city.
     */
    double getLongitude();

    /**! getLatitude()
     * \brief get the latitude of the city.
     * \detail The latitude is purely in degrees no minutes or seconds
     * it has a sign (+/-) instead of N/S/E/W.
     * \return longitude of the position of the city.
     */
    double getLatitude();

    /**! setLongitude(double)
     * \brief set the longitude of the city to a new value.
     * \param longitude longitude is purely in degrees no minutes or seconds
     * it has a sign (+/-) instead of N/S/E/W eg (-0.1234) or (54.1243)
     * \see CityMaths (for 'flattenDegrees' function which converts minutes
     * and seconds into pure degrees).
     */
    void setLongitude(double longitude);

    /**! setLatitude(double)
     * \brief set the latitude of the city to a new value.
     * \param latitude latitude is purely in degrees no minutes or seconds
     * it has a sign (+/-) instead of N/S/E/W eg (-0.1234) or (54.1243)
     * \see CityMaths (for 'flattenDegrees' function which converts minutes
     * and seconds into pure degrees).
     */
    void setLatitude(double latitude);
};
		
#endif