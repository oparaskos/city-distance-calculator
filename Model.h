/* Model Interface
 * Author:
 * Katie Jarvis
 */
#ifndef MODEL_H
#define MODEL_H

#include "Singleton.hpp"
#include "CityLoader.h"
#include <string>

class Model : public Singleton<Model>
{
public:
    Model(){}
    virtual ~Model(){}
    
    virtual bool addCity(City city)=0;
    virtual bool removeCity(City city)=0;
    virtual bool modifyCity(std::string, City)=0;
    virtual bool loadCities(std::string path)=0;
    virtual bool saveCities(std::string path)=0;
    virtual CityLoader::citymap::iterator findCity(string cityName)=0;
    virtual double getDistance(string cityA, string cityB)=0;
    virtual void showCityInfo(string cityName)=0;
    virtual void showAll()=0;
    virtual CityLoader::citymap::iterator end()=0;
    virtual CityLoader::citymap::iterator begin()=0;
    virtual unsigned int size()=0;
};

#endif