/* 
 * File:   CityLoader.h
 * Author: Oliver Paraskos
 *
 * Created on 02 October 2013, 16:33
 */

#ifndef CITYLOADER_H
#define	CITYLOADER_H

#include <iostream>
#include <string>
#include <unordered_map>
#include <cstdlib>
#include <cstring>
#include <sstream>
#include <fstream>
#include "CityMaths.h"
#include "City.h"

/*---------------------- HELPER FUNCTIONS ---------------------*/
        /** @todo Move into their own helpers.h */
void replace(std::string& inStr, char find, char replace);
bool isEmpty(std::string inStr);
std::string trim(const std::string& inStr,
                 const std::string& trim = " \t");
/*-------------------------------------------------------------*/

/**
 * namespace containing methods for loading of city coordinates
 * 
 * @detail
 * Expected city file Format:
 * city name LONGITUDE....... LATITUDE........
 * city_name DEG_MIN_SEC_SIGN DEG_MIN_SEC_SIGN
 * 
 * spaces in strings must be replaced with underscores.
 * MIN and SEC are optional fields.
 * SIGN is N, S, E, W;
 */
namespace CityLoader
{
	/* no-one wants to type all this stuff every time, thats what a 
	 * typedef is for so use 'citymap' as the type name instead of
	 * 'std::unordered_map<std::string, City>' */
	typedef std::unordered_map<std::string, City> citymap;

	/**
	 * Interpret a string with long or lat coordinates in it in degrees
	 * minutes and seconds with a sign.
	 * 
	 * @detail string format must have degrees minutes seconds separated 
	 * with spaces or underscores. minutes and seconds are optional.
	 * 
	 * @param str the input string to read from
	 * @param &degrees output degrees from the string
	 * @param &minutes output minutes from the string
	 * @param &seconds output seconds from the string
	 * @param &sign	N,S,E,W sign of the longitude/latitude */
	void readCoordinate(std::string str, double &degrees, double &minutes, 
		double &seconds, char &sign );
	
	
	
	/**
	 * Function to parse a line from the file (a city)
	 * @detail N and E are implicit if sign is not given.
	 * @return a new City with information filled in.
	 */
	City parseCity(std::string StrCityInfo);
	
	/**! loadFile
	 * \brief Load a list of cities from a file and put it in an 
	 * unordered_map.
	 * 
	 * \param StrFileToLoad the filepath of the file to load.
	 */
	citymap loadFile(std::string const & StrFileToLoad);
        /** saveFile
	 *  Load a list of cities from a file and put it in an 
	 * unordered_map.
	 * 
	 * @param StrFileToLoad the filepath of the file to load.
	 */
	bool saveFile(std::string const & infile);
}

#endif	/* CITYLOADER_H */

