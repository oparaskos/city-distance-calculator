/* 
 * Authors:
 *  Emogene Atkinson - 12015483
 *  Katie Jarvis - 12021767
 * 
 * See Self-documenting comments in the header file for 
 * more information.
 */

#include "City.h"

string City::getName(){
	return name;
}

void City::setName(string name){
	this->name=name;
}
double City::getLongitude(){
	return longitude;
}
double City::getLatitude(){
	return latitude;
}
void City::setLongitude(double longitude){
	this->longitude=longitude;
}
void City::setLatitude(double latitude){
	this->latitude=latitude;
}
		
		