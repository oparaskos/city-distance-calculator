/* Command Line View Source File
 * Author:
 * Emogene Atkinson
 */
 
#include "CommandLineView.h"
#include "ErrorStack.h"

inline void printErrors()
{   
    while(ErrorStack::GetSingleton().size()) {
        std::cerr << ErrorStack::GetSingleton().pop().getMessage();
    }
}

//Outputs a message.
void CommandLineView::printMessage(std::string message)
{
    printErrors();
    std::cout << message;
}

/*@todo handle unsigned/signed ints ...*/
View & operator<<(View& o, const std::string & message)
{
    o.printMessage(message);
    return o;
}
View & operator<<(View& o, const double & d)
{
    char buf[27];
    sprintf(buf, "%f", d);
    o.printMessage(std::string(buf));
    return o;
}

View & operator<<(View& o, const unsigned int & d)
{ 
    //char buf[27];
    //sprintf(buf, "%d", d);
    //o.printMessage(std::string(buf));
	return operator<<(o, (int)d);//o;
}

View & operator<<(View& o, const int & d)
{
    char buf[27];
    sprintf(buf, "%d", d);
    o.printMessage(std::string(buf));
    return o;
}