#include <iostream>
#include "CityDistanceModel.h"
#include "CommandLineController.h"
#include "CommandLineView.h"
#include "ErrorStack.h"

int main(int argc, char** argv)
{
    /* Create a model, view and controller*/
    ErrorStack errorStack;
    CityDistanceModel cityDistanceModel;
    CommandLineView commandLineView;
    CommandLineController commandLineController;
    
    /*make them singletons, globally available*/
    ErrorStack::SetSingletonPtr(&errorStack);
    Model::SetSingletonPtr(&cityDistanceModel);
    View::SetSingletonPtr(&commandLineView);
    Controller::SetSingletonPtr(&commandLineController);
    
    /* give the controller control*/
    Controller::GetSingleton().control();
    
    return 0;
}